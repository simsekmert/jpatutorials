package com.test;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class TestBean
 */
@Singleton
@LocalBean
@Startup
public class TestBean {

	@PersistenceContext(name = "ManyToManyTut")
	EntityManager em;
	
    /**
     * Default constructor. 
     */
    public TestBean() {
        // TODO Auto-generated constructor stub
    }
    
    @PostConstruct
    public void main(){
    	
    	//Creates projects
    	Project project1 = new Project();
    	Project project2 = new Project();
    	
    	project1.setName("Android Project");
    	project2.setName("Ios Project");
    	
    	//Creates employees
    	Employee employee1 = new Employee();
    	Employee employee2 = new Employee();
    	
    	employee1.setName("Mert");
    	employee2.setName("Ahmet");
    	
    	//Define list by created projects
    	List<Project> project_list = new ArrayList<>();
    	project_list.add(project1);
    	project_list.add(project2);
    
    	
    	//Define list by created employees
    	List<Employee> employee_list = new ArrayList<>();
    	employee_list.add(employee1);
    	employee_list.add(employee2);
    	
    	//assign both employees to all projects
    	employee1.setProjects(project_list);
    	employee2.setProjects(project_list);
    	
    	//assign all projects to both employees
    	project1.setEmployees(employee_list);
    	project2.setEmployees(employee_list);
    	
    	em.persist(employee2);
    	
    }

}
