package model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the food_item database table.
 * 
 */
@Entity
@Table(name="food_item")
@NamedQuery(name="FoodItem.findAll", query="SELECT f FROM FoodItem f")
public class FoodItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="food_item_id")
	private int foodItemId;

	private String name;

	private int quantity;

	@Column(name="supplier_id")
	private int supplierId;

	//bi-directional many-to-many association to Animal
	@ManyToMany(mappedBy="foodItems",cascade = CascadeType.PERSIST)
	private List<Animal> animals = new ArrayList<>();

	public FoodItem() {
	}

	public int getFoodItemId() {
		return this.foodItemId;
	}

	public void setFoodItemId(int foodItemId) {
		this.foodItemId = foodItemId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public List<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(List<Animal> animals) {
		this.animals = animals;
	}

}