package model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@Table(name = "animal")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="animal_id")
	private int animalId;

	@Column(name="category_id")
	private int categoryId;

	@Column(name="total_no")
	private int totalNo;

	private String type;

	//bi-directional one-to-one association to Cage
	@OneToOne(mappedBy="animal")
	private Cage cage;

	//bi-directional many-to-many association to FoodItem
	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable(
		name="animal_food_item"
		, joinColumns={
			@JoinColumn(name="animal_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="food_item_id")
			}
		)
	private List<FoodItem> foodItems = new ArrayList<>();

	public Animal() {
	}

	public int getAnimalId() {
		return this.animalId;
	}

	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getTotalNo() {
		return this.totalNo;
	}

	public void setTotalNo(int totalNo) {
		this.totalNo = totalNo;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Cage getCage() {
		return this.cage;
	}

	public void setCage(Cage cage) {
		this.cage = cage;
	}

	public List<FoodItem> getFoodItems() {
		return this.foodItems;
	}

	public void setFoodItems(List<FoodItem> foodItems) {
		this.foodItems = foodItems;
	}

}