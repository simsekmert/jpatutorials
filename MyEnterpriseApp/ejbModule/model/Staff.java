package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the staff database table.
 * 
 */
@Entity
@Table(name = "staff")
public class Staff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="staff_id")
	@TableGenerator(name="staffTableGen", pkColumnName="gen_name",
					valueColumnName="gen_value", pkColumnValue="staff_id_gen",
					table="id_gen",initialValue=50 ,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="staffTableGen")
	private int staffId;

	private int age;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;

	private String name;

	private int salary;
	
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date start_date;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] pic;

	public Staff() {
	}

	public int getStaffId() {
		return this.staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return this.salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public byte[] getPic() {
		return pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	
	
}