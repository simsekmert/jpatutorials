package main;

import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Animal;
import model.Cage;
import model.FoodItem;
import model.Gender;
import model.Staff;

/**
 * Session Bean implementation class Test
 */
@Singleton
@LocalBean
@Startup
public class Test {

	@PersistenceContext(name = "MyEnterpriseApp")
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public Test() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void main() {
		
		Animal animal1 = new Animal();
		Animal animal2 = new Animal();
		
		animal1.setType("animal1");
		animal1.setTotalNo(2);
		animal2.setType("animal2");
		animal2.setTotalNo(2);
		
		FoodItem foodItem1 = new FoodItem();
		FoodItem foodItem2 = new FoodItem();
		
		foodItem1.setName("food1");
		foodItem2.setName("food2");
		
		//animal1.getFoodItems().add(foodItem1);
		//animal1.getFoodItems().add(foodItem2);
		
		//animal2.getFoodItems().add(foodItem1);
		//animal2.getFoodItems().add(foodItem2);
		
		foodItem1.getAnimals().add(animal1);
		foodItem1.getAnimals().add(animal2);
		
		em.persist(foodItem1);
		
	}

}
