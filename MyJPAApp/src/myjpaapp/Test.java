package myjpaapp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Member;

public class Test {
	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("MyJPAApp");
		EntityManager em = emf.createEntityManager();

		//Creates new entity end insert into DB.
		em.getTransaction().begin();

		Member staff = new Member();
		staff.setAge(23);
		staff.setIsim("Halime");
		staff.setSalary(2000);
		staff.setGender("Female");

		em.persist(staff);

		em.getTransaction().commit();
		
		//******************************
		
		//Read data from database
		//Staff staff = em.find(Staff.class, 1);
		//System.out.println("Staff' name is " + staff.getName());
		
		//******************************
		
		//Update data on DB
		//em.getTransaction().begin();
		
		//Staff staff = em.find(Staff.class, 1);
		//staff.setSalary(3000);
		
		//em.getTransaction().commit();
		
		//******************************
		
		//Delete data from DB
		//em.getTransaction().begin();
		
		//Staff staff = em.find(Staff.class, 1);
		//em.remove(staff);
		
		//em.getTransaction().commit();
		

		em.close();
		emf.close();
	}

}
